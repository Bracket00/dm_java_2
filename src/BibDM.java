import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.*;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a + b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
      if(liste.size()!=0){
        Integer min = liste.get(0);
        for (Integer i : liste){
          if (i < min){
            min = i;
          }
        }
        return min;
      }
        return null;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
      for(T t :liste){
        if(t.compareTo(valeur) <0){
          return false;
        }
      }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
      List<T>listeF = new ArrayList<>(liste1);
      for (T t: liste2){
        if(listeF.contains(t) == false){
          listeF.add(t);
        }
      }
        return listeF;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
      String[] liste = texte.split(" ");
      List <String> listeStr = new ArrayList<>();
      for(String str : liste){
        listeStr.add(str);
      }
      return listeStr;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        Map <String, Integer> dico = new HashMap <> ();
        List <String> liste = new ArrayList<> (decoupe(texte));
        for(String s: liste){
          Integer nombre = dico.get(s);
            if (nombre == null){
                dico.put(s,1);
            }
            else {
                dico.put(s,nombre + 1);
            }
        }
        Map.Entry<String,Integer>maxEntry = null;
        for(Map.Entry<String, Integer> map : dico.entrySet()){
          if(maxEntry == null || map.getValue().compareTo(maxEntry.getValue())>0){
            maxEntry = map;
          }
        }

        return maxEntry.getKey();
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int compteurP = 0;
        for(int i = 0; i < chaine.length();++i){
          if(chaine.charAt(i) == '('){
            compteurP += 1;
          }
          else if (chaine.charAt(i) == ')') {
            if(compteurP == 0)
            {
              return false;
            }
            else
            {
              compteurP -=1;
            }
          }
        }
        if(compteurP == 0){
          return true;
        }
        return false;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        return true;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
      Integer bas = 0;
      Integer haut = liste.size()-1;
      Integer milieu;
      while(bas < haut)
      {
        milieu = (bas + haut)/2;
        if (liste.get(milieu)<valeur)
        {
          bas += 1;
        }
        else
        {
          haut = milieu;
        }
      }
      return (bas < liste.size() && valeur.equals(liste.get(bas)));
      }
}
